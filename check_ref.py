#!/usr/bin/env python
from __future__ import print_function

from astropy.io import fits
from crds import rmap, data_file
import argparse
import sys
from jwst_pipeline.dq_init import DQInitStep
from jwst_pipeline.saturation import SaturationStep
from jwst_pipeline.ipc import IPCStep
from jwst_pipeline.superbias import SuperBiasStep
from jwst_pipeline.refpix import RefPixStep
from jwst_pipeline.reset import ResetStep
from jwst_pipeline.lastframe import LastFrameStep
from jwst_pipeline.linearity import LinearityStep
from jwst_pipeline.dark_current import DarkCurrentStep
from jwst_pipeline.jump import JumpStep
from jwst_pipeline.ramp_fitting import RampFitStep

from jwst_pipeline.assign_wcs import AssignWcsStep
from jwst_pipeline.photom import PhotomStep
from jwst_pipeline.flatfield import FlatFieldStep
from jwst_pipeline.straylight import StraylightStep
from jwst_pipeline.fringe import FringeStep


def main(args):

    # use colors if writing to terminal
    if sys.stdout.isatty():
        RED = '\033[91m'
        GREEN = '\033[92m'
        YELLOW = '\033[93m'
        ENDC = '\033[0m'
    else:
        RED = ''
        GREEN = ''
        YELLOW = ''
        ENDC = ''

    steps = {
        'mask' : ([DQInitStep], {'override_mask' : args.reference_file}),
        'saturation' : ([SaturationStep], {'override_saturation' : args.reference_file}),
        'ipc' : ([IPCStep], {'override_ipc' : args.reference_file}),
        'superbias' : ([SuperBiasStep], {'override_superbias' : args.reference_file}),
        'refpix' : ([RefPixStep], {'override_refpix' : args.reference_file}),
        'reset' : ([ResetStep], {'override_reset' : args.reference_file}),
        'lastframe' : ([LastFrameStep], {'override_lastframe' : args.reference_file}),
        'linearity' : ([LinearityStep], {'override_linearity' : args.reference_file}),
        'dark' : ([DarkCurrentStep], {'override_dark' : args.reference_file}),
        'gain' : ([JumpStep], {'override_gain' : args.reference_file}),
        'readnoise' : ([[JumpStep], RampFitStep], {'override_readnoise' : args.reference_file}),

        'wcs' : ([AssignWcsStep], {'override_wcs' : args.reference_file}),
        'flat' : ([FlatFieldStep], {'override_flat' : args.reference_file}),
        'area' : ([PhotomStep], {'override_area' : args.reference_file}),
        'fringe' : ([FringeStep], {'override_fringe' : args.reference_file}),
        'straymask' : ([StraylightStep], {'override_straymask' : args.reference_file}),
        'photom' : ([PhotomStep, {'override_photom' : args.reference_file}]),

    }

    # get header dictionary in proper format for rmap
    header = data_file.get_data_model_header(args.reference_file)
    reftype = header['REFTYPE'].lower()
    if args.science is not None:
        science_file = args.science
    else:
        pmap = rmap.load_mapping('/user/mhill/check_ref/mappings/jwst_check_ref_0001.pmap', ignore_checksum=True)
        best_refs = pmap.get_best_references(header)
        science_file = best_refs[reftype]

    print("Using reference file: "+args.reference_file)
    if science_file == 'NOT FOUND n/a' or science_file == 'NOT FOUND No match found.':
        print(YELLOW+"SKIPPED"+ENDC+' - '+"No matching science file available for reference file with parameters:"+'\n')
        imap = pmap.get_imap(header['META.INSTRUMENT.NAME'])
        rmap_context = imap.get_rmap(header['REFTYPE'])
        min_params = rmap_context.get_minimum_header(args.reference_file)
        print('\t'+"REFTYPE : "+header['REFTYPE'])
        for key, val in zip(min_params.iterkeys(), min_params.itervalues()):
            print('\t'+key+" : "+val)
        print()
        return 

    print("Using science file: "+science_file)
    
    try:
        PipelineSteps, kwargs = steps[reftype]
        for PipelineStep in PipelineSteps:
            print("Running "+str(PipelineStep))
            science_file = PipelineStep.call(science_file, **kwargs)
            print(GREEN+"SUCCESS"+ENDC)
            print()

    except Exception as error:
        print(RED+"FAILED"+ENDC+' - '+str(error))
        print()
    

if __name__ == '__main__':
   parser = argparse.ArgumentParser(description="Check that a new reference file doesn't cause a pipeline crash")
   parser.add_argument('reference_file', help='reference file to check')
   parser.add_argument('-s', '--science', help='science file to process with reference file')
   args = parser.parse_args()
   main(args)