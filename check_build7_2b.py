#!/usr/bin/env python

from __future__ import print_function

import argparse
import sys
import os
import glob
import multiprocessing as mp
from collections import OrderedDict
import datetime

from astropy.io import fits
import crds
import jwst
from jwst.datamodels import RampModel
from jwst.pipeline.calwebb_sloper import SloperPipeline
from jwst.dq_init import DQInitStep
from jwst.saturation import SaturationStep
from jwst.superbias import SuperBiasStep
from jwst.refpix import RefPixStep
from jwst.rscd import RSCD_Step
from jwst.lastframe import LastFrameStep
from jwst.linearity import LinearityStep
from jwst.dark_current import DarkCurrentStep
from jwst.persistence import PersistenceStep
from jwst.jump import JumpStep
from jwst.ramp_fitting import RampFitStep

from jwst.assign_wcs import AssignWcsStep
from jwst.flatfield import FlatFieldStep
from jwst.photom import PhotomStep

from jwst.background import BackgroundStep
from jwst.extract_2d import Extract2dStep
from jwst.imprint import ImprintStep
from jwst.straylight import StraylightStep
from jwst.fringe import FringeStep

def get_keyword(keyword, header):
    if keyword in header:
        return str(header[keyword])
    else:
        return 'Not Present'

def run_pipeline(args):

    fname, report = args

    base = fname.split('uncal')[0]
    print("Beginning "+fname)

    header = fits.getheader(fname)
    date = get_keyword('DATE-OBS', header)
    time = get_keyword('TIME-OBS', header)
    instrument = get_keyword('INSTRUME', header)
    exp_type = get_keyword('EXP_TYPE', header)
    detector = get_keyword('DETECTOR', header)
    readpatt = get_keyword('READPATT', header)
    nints = get_keyword('NINTS', header)
    ngroups = get_keyword('NGROUPS', header)
    filter = get_keyword('FILTER', header)
    subarray = get_keyword('SUBARRAY', header)
    pupil = get_keyword('PUPIL', header)
    grating = get_keyword('GRATING', header)


    steps = OrderedDict()
    steps['dq_init'] = DQInitStep
    steps['saturation'] = SaturationStep
    steps['superbias'] = SuperBiasStep
    steps['refpix'] = RefPixStep
    steps['rscd'] = RSCD_Step
    steps['lastframe'] = LastFrameStep
    steps['linearity'] = LinearityStep
    steps['dark_current'] = DarkCurrentStep
    steps['persistence'] = PersistenceStep
    steps['jump'] = JumpStep
    steps['ramp_fit'] = RampFitStep

    is_spec = False
    if (header['INSTRUME'] == 'NIRSPEC') or ('MIRIFU' in header['DETECTOR']) or ('WFSS' in header['EXP_TYPE']):
        is_spec = True

    if not ('SOSS' in header['EXP_TYPE']): 
        steps['assign_wcs'] = AssignWcsStep
    if is_spec:
        #steps['background'] = BackgroundStep
        #steps['imprint'] = ImprintStep
        steps['extract_2d'] = Extract2dStep

    steps['flatfield'] = FlatFieldStep

    if is_spec:
        steps['straylight'] = StraylightStep
        steps['fringe'] = FringeStep

    steps['photom'] = PhotomStep


    output = RampModel(os.path.abspath(fname))

    for step_name, Step in steps.iteritems():
        try:
            if step_name == 'ramp_fit':
                output = Step.call(output, output_file=base+step_name+'.fits', save_opt=False, int_name=base+'rateints.fits')
            elif step_name == 'photom':
                output = Step.call(output, output_file=base+step_name+'.fits')
                with open(report, 'a') as f:
                    f.write(os.path.dirname(fname)+';'+date+';'+time+';'+instrument+';'+exp_type+';'+detector+';'+subarray+';'+readpatt+';'+nints+';'+ngroups+';'+filter+';'+pupil+';'+grating+";SUCCESS; \n")
            else:
                # output = Step.call(output, output_file=base+step_name+'.fits')
                output = Step.call(output)
        except Exception as error:
            with open(report, 'a') as f:
                f.write(os.path.dirname(fname)+';'+date+';'+time+';'+instrument+';'+exp_type+';'+detector+';'+subarray+';'+readpatt+';'+nints+';'+ngroups+';'+filter+';'+pupil+';'+grating+";FAILED;"+step_name+" - "+str(error)+'\n')
            return

def main(args):
    if args.nproc is not None:
        p = mp.Pool(args.nproc)
    else:
        p = mp.Pool(mp.cpu_count())

    _, crds_context = crds.heavy_client.get_processing_mode("jwst")
    with open(args.report, 'a') as f:
        f.write("# CRDS_CONTEXT = '{}'\n".format(crds_context))
        f.write("# run date = {}\n".format(datetime.datetime.now().isoformat()))
        f.write("# cal version = {}\n".format(jwst.__version__))
    files = open(args.file_list).read().splitlines()
    p.map(run_pipeline, zip(files, [args.report]*len(files)))
    

if __name__ == '__main__':
   parser = argparse.ArgumentParser(description="Check build 7 2B pipeline doesn't crash")
   parser.add_argument('file_list', help='list of files to run the pipeline on')
   parser.add_argument('report', help='where to save the results')
   parser.add_argument('--nproc', help='number of cores default is all of them', type=int)
   args = parser.parse_args()
   main(args)